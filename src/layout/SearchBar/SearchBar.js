import React from "react";

import { withStyles } from "@material-ui/core/styles";

import { InputBase, IconButton } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";

const styles = theme => ({
  whiteColor: {
    color: "white"
  }
});

class SearchBar extends React.Component {

  state = {
    searchStr: ""
  };

  searchClickHandler = () => {
    this.props.searchStrChangeHandler(this.state.searchStr);
  };

  searchStrChangeHandler = e => {
    this.setState({ searchStr: e.target.value });
  };

  render() {
    return (
      <React.Fragment>
        <InputBase className={this.props.classes.whiteColor} value={this.state.searchStr} onChange={this.searchStrChangeHandler} placeholder="Search" />
        <IconButton className={this.props.classes.whiteColor}  onClick={this.searchClickHandler} aria-label="Search">
          <SearchIcon />
        </IconButton>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(SearchBar);
