import React from "react";

import { withStyles } from "@material-ui/core/styles";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

import SearchBar from "../SearchBar/SearchBar";

const styles = theme => ({
  flex1: {
    flex: 1
  }
});

const NavBar = props => {
  const { classes } = props;
  return (
    <AppBar position="static" color="primary">
      <Toolbar>
        <Typography className={classes.flex1} variant="h5" color="inherit">
          GitHub
        </Typography>
        <SearchBar searchStrChangeHandler={props.searchStrChangeHandler} />
      </Toolbar>
    </AppBar>
  );
};

export default withStyles(styles)(NavBar);
