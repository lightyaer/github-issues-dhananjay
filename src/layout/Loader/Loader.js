import React from "react";
import { withStyles } from "@material-ui/core/styles";

import { CircularProgress } from "@material-ui/core";

const styles = theme => ({
  progressContainer: {
    position: "relative",
    height: "100vh",
    width: "100%",
    backgroundColor: "#3a393980",
    zIndex: "99"
  },
  progress: {
    position: "absolute",
    left: "50%",
    top: "35%",
    zIndex: "99"
  }
});

const Loader = props => (
  <div className={props.classes.progressContainer}>
    <CircularProgress className={props.classes.progress} color="primary" />
  </div>
);

export default withStyles(styles)(Loader);
