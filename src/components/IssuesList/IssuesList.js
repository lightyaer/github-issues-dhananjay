import React from "react";

import { Paper, List } from "@material-ui/core";

import IssuesToolBar from "../IssuesToolBar/IssuesToolBar";
import IssuesListItem from "./IssueListItem/IssueListItem";

import { getIssues, getLabels, searchIssues } from "../../services/IssueService";
import Loader from "../../layout/Loader/Loader";

class IssuesList extends React.Component {
  state = {
    isLoading: true,
    page: 0,
    rowsPerPage: 10,
    direction: "desc",
    sort: "created",
    sortById: 1,
    sortBys: [],
    state: "open",
    states: [],
    selectedAuthorId: 0,
    selectedAuthor: null,
    authors: {},
    selectedLabelId: 0,
    selectedLabel: null,
    labels: [],
    searchQuery: "",
    issues: []
  };

  getAuthors = issues => {
    return issues.reduce((result, issue) => {
      if (!result.hasOwnProperty(issue.user.id)) {
        result[issue.user.id] = issue.user.login;
      }
      return result;
    }, {});
  };

  async componentDidUpdate(prevProps) {
    try {
      if (this.props.searchQuery !== prevProps.searchQuery) {
        if (this.props.searchQuery === "") {
          const issues = await getIssues({
            sort: this.state.sort,
            direction: this.state.direction,
            creator: this.state.selectedAuthor,
            labels: this.state.selectedLabel,
            state: this.state.state
          });
          this.setState({ issues });
        } else {
          let query = this.props.searchQuery;
          let qualifier = "repo:Lawrence4code/github-issues-clone";
          let searchQuery = `${query}+${qualifier}`;
          let issues = await searchIssues(searchQuery);
          issues = issues.items;
          this.setState({ issues });
        }
      }
    } catch (error) {
      throw error;
    }
  }

  async componentDidMount() {
    try {
      const labels = await getLabels();
      const issues = await getIssues({
        sort: this.state.sort,
        direction: this.state.direction,
        creator: this.state.selectedAuthor,
        labels: this.state.selectedLabel,
        state: this.state.state
      });
      const authors = this.getAuthors(issues);
      const sortBys = [
        { direction: "desc", name: "Newest", sort: "created", id: 1 },
        { direction: "asc", name: "Oldest", sort: "created", id: 2 },
        { direction: "desc", name: "Recently Updated", sort: "updated", id: 3 },
        { direction: "asc", name: "Least Recently Updated", sort: "updated", id: 4 }
      ];
      const states = [{ id: 1, value: "all", name: "All" }, { id: 2, value: "open", name: "Open" }, { id: 3, value: "closed", name: "Closed" }];
      this.setState({ issues, labels, authors, sortBys, states, isLoading: false });
    } catch (error) {}
  }

  sortByChangeHandler = async e => {
    this.setState({ isLoading: true });
    let { sort, direction, id } = e.currentTarget.dataset;
    console.log(sort, direction, id);
    const issues = await getIssues({
      sort,
      direction,
      state: this.state.state,
      labels: this.state.selectedLabel,
      creator: this.state.selectedAuthor
    });
    this.setState({ issues, sort, direction, sortById: id, isLoading: false });
  };

  stateChangeHandler = async e => {
    this.setState({ isLoading: true });

    let state = e.target.value;
    console.log(state);
    const issues = await getIssues({
      sort: this.state.sort,
      direction: this.state.direction,
      state,
      labels: this.state.selectedLabel,
      creator: this.state.selectedAuthor
    });
    this.setState({ issues, state, isLoading: false });
  };

  authorChangeHandler = async e => {
    this.setState({ isLoading: true });

    let authorId = +e.target.value;
    let { name } = e.currentTarget.dataset;
    const issues = await getIssues({
      sort: this.state.sort,
      direction: this.state.direction,
      state: this.state.state,
      creator: name,
      labels: this.state.selectedLabel
    });
    this.setState({ issues, selectedAuthorId: authorId, selectedAuthor: name, isLoading: false });
  };

  labelChangeHandler = async e => {
    this.setState({ isLoading: true });

    let labelId = +e.target.value;
    let { label } = e.currentTarget.dataset;
    const issues = await getIssues({
      sort: this.state.sort,
      direction: this.state.direction,
      state: this.state.state,
      creator: this.state.selectedAuthor,
      labels: label
    });
    this.setState({ issues, selectedLabel: label, selectedLabelId: labelId, isLoading: false });
  };

  firstPage = async () => {
    this.setState({ isLoading: true });

    const issues = await getIssues({
      sort: this.state.sort,
      direction: this.state.direction,
      labels: this.state.selectedLabel,
      creator: this.state.selectedAuthor,
      page: 1
    });
    this.setState({ issues, page: 1, isLoading: false });
  };

  nextPage = async () => {
    this.setState({ isLoading: true });

    let page = this.state.page;
    page += 1;
    const issues = await getIssues({
      sort: this.state.sort,
      direction: this.state.direction,
      labels: this.state.selectedLabel,
      creator: this.state.selectedAuthor,
      page
    });
    this.setState({ issues, page, isLoading: false });
  };

  previousPage = async () => {
    this.setState({ isLoading: true });

    let page = this.state.page;
    page > 1 ? (page -= 1) : (page = 1);
    const issues = await getIssues({
      sort: this.state.sort,
      direction: this.state.direction,
      labels: this.state.selectedLabel,
      creator: this.state.selectedAuthor,
      page
    });
    this.setState({ issues, page, isLoading: false });
  };

  render() {
    const loading = this.state.isLoading ? <Loader /> : null;

    return (
      <React.Fragment>
        <Paper>
          <IssuesToolBar
            sortById={this.state.sortById}
            sortByChangeHandler={this.sortByChangeHandler}
            sortBys={this.state.sortBys}
            state={this.state.state}
            stateChangeHandler={this.stateChangeHandler}
            states={this.state.states}
            selectedAuthorId={this.state.selectedAuthorId}
            authorChangeHandler={this.authorChangeHandler}
            authors={this.state.authors}
            selectedLabelId={this.state.selectedLabelId}
            labelChangeHandler={this.labelChangeHandler}
            labels={this.state.labels}
            nextPage={this.nextPage}
            previousPage={this.previousPage}
            firstPage={this.firstPage}
          />
          {loading}
          <List dense>
            {this.state.issues.map(issue => (
              <IssuesListItem key={issue.id} issue={issue} />
            ))}
          </List>
        </Paper>
      </React.Fragment>
    );
  }
}

export default IssuesList;
