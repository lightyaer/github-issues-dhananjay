import React from "react";
import moment from "moment";
import { withStyles } from "@material-ui/core/styles";
import { ListItem, ListItemText, ListItemAvatar, Avatar, Chip } from "@material-ui/core";
import LabelIcon from "@material-ui/icons/Label";

import { Link } from "react-router-dom";

const styles = theme => ({
  link: {
    textDecoration: "none"
  },
  listItem: {
    justifyContent: "space-between"
  }
});

const IssueListItem = props => {
  let { classes } = props;
  const labels = props.issue.labels.map(label => {
    return (
      <Chip
        key={label.id}
        icon={<LabelIcon style={{ color: "white" }} />}
        style={{
          backgroundColor: "#" + label.color,
          color: "white",
          marginRight: "0.2rem",
          marginTop: "0.2rem"
        }}
        label={label.name}
      />
    );
  });

  return (
    <ListItem className={classes.listItem} key={props.issue.id} button>
      <Link to={`/issues/${props.issue.number}`} className={classes.link}>
        <ListItemText
          primary={props.issue.title}
          secondary={`#${props.issue.number}
      Created ${moment(props.issue.created_at).fromNow()},
      Updated ${moment(props.issue.updated_at).fromNow()}
      by ${props.issue.user.login} 
      `}
        />
      </Link>
      <div style={{ flexWrap: "wrap" }}> {labels}</div>
      <ListItemAvatar onClick={() => props.gotoUserProfile(props.issue.user.html_url)}>
        <Avatar src={props.issue.user.avatar_url} />
      </ListItemAvatar>
    </ListItem>
  );
};

export default withStyles(styles)(IssueListItem);
