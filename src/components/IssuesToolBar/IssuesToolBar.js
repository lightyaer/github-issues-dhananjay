import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Toolbar, FormControl, InputLabel, Select, MenuItem, Tooltip, IconButton } from "@material-ui/core";

import NextIcon from "@material-ui/icons/NavigateNext";
import PreviousIcon from "@material-ui/icons/NavigateBefore";
import FirstPageIcon from "@material-ui/icons/FirstPage";

const styles = theme => ({
  formControl: {
    minWidth: 150,
    marginRight: "1rem"
  }
});

const IssuesToolBar = props => {
  let { classes } = props;

  return (
    <React.Fragment>
      <Toolbar>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="sort-by">Sort By</InputLabel>
          <Select
            value={props.sortById}
            onChange={props.sortByChangeHandler}
            inputProps={{
              name: "sortby",
              id: "sort-by"
            }}
          >
            {props.sortBys.map(item => (
              <MenuItem key={item.id} data-sort={item.sort} value={item.id} data-id={item.id} data-direction={item.direction}>
                {item.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="issue-state">State</InputLabel>
          <Select
            value={props.state}
            onChange={props.stateChangeHandler}
            inputProps={{
              name: "issueState",
              id: "issue-state"
            }}
          >
            {props.states.map(item => (
              <MenuItem key={item.id} value={item.value}>
                {item.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="issue-author">Author</InputLabel>
          <Select
            value={props.selectedAuthorId}
            onChange={props.authorChangeHandler}
            inputProps={{
              name: "issueAuthor",
              id: "issue-author"
            }}
          >
            <MenuItem value={0}>All</MenuItem>
            {Object.keys(props.authors).map(authorId => (
              <MenuItem key={authorId} data-name={props.authors[authorId]} value={authorId}>
                {props.authors[authorId]}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="issue-label">Label</InputLabel>
          <Select
            value={props.selectedLabelId}
            onChange={props.labelChangeHandler}
            inputProps={{
              name: "issueLabel",
              id: "issue-label"
            }}
          >
            <MenuItem value={0}>All</MenuItem>
            {props.labels.map(label => (
              <MenuItem key={label.id} data-label={label.name} value={label.id}>
                {label.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <Tooltip ml={4} title="First">
          <IconButton onClick={props.firstPage}>
            <FirstPageIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="Previous">
          <IconButton onClick={props.previousPage}>
            <PreviousIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="Next">
          <IconButton onClick={props.nextPage}>
            <NextIcon />
          </IconButton>
        </Tooltip>
      </Toolbar>
    </React.Fragment>
  );
};

export default withStyles(styles)(IssuesToolBar);
