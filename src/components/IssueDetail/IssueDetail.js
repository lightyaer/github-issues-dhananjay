import React from "react";
import { withStyles } from "@material-ui/core/styles";
import moment from "moment";
import { Markdown } from "react-showdown";
import LabelIcon from "@material-ui/icons/Label";
import DeleteIcon from "@material-ui/icons/Delete";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import {
  AppBar,
  Typography,
  Toolbar,
  Chip,
  Card,
  CardContent,
  CardHeader,
  Avatar,
  TextField,
  Button,
  FormControl,
  InputLabel,
  Select,
  Input,
  MenuItem,
  IconButton
} from "@material-ui/core";

import { getIssueById, getComments, postComment, deleteComment, editIssue, getLabels } from "../../services/IssueService";
import Loader from "../../layout/Loader/Loader";

const styles = theme => ({
  commentCard: {
    margin: "1rem 2rem"
  },
  issueCard: {
    margin: "1rem"
  },
  labels: {
    textAlign: "center"
  },
  newComment: {
    width: "100%"
  },
  button: {
    width: "100%"
  },
  addLabel: {
    minWidth: 150,
    marginLeft: "2rem",
    marginRight: "2rem",
    float: "right"
  },
  backButton: {
    marginRight: "1rem"
  }
});
class IssueDetail extends React.Component {
  state = {
    issue: {},
    comments: [],
    labels: [],
    isLoading: true,
    newComment: "",
    newLabel: ""
  };
  async componentDidMount() {
    let { issueId } = this.props.match.params;
    const issue = await getIssueById(issueId);
    const labels = await getLabels();
    const comments = await getComments(issueId);
    this.setState({ issue, comments, labels, isLoading: false });
  }

  goBack = () => {
    this.props.history.goBack();
  };

  newCommentChangeHandler = e => {
    this.setState({ newComment: e.target.value });
  };

  addCommentHandler = async () => {
    try {
      this.setState({ isLoading: true });
      if (this.state.newComment) {
        let comment = await postComment(this.state.issue.number, this.state.newComment);
        let comments = [...this.state.comments];
        comments.push(comment);
        this.setState({ comments, newComment: "", isLoading: false });
      }
    } catch (error) {
      throw error;
    }
  };

  deleteCommentHandler = async commentId => {
    try {
      this.setState({ isLoading: true });
      await deleteComment(commentId);
      let comments = [...this.state.comments];
      comments = comments.filter(comment => comment.id !== commentId);
      this.setState({ comments, isLoading: false });
    } catch (error) {
      throw error;
    }
  };

  editIssueHandler = async (action, labelName) => {
    try {
      switch (action) {
        case "add": {
          let addedLabels = this.state.issue.labels.map(label => label.name);
          if (addedLabels.includes(labelName)) return;
          addedLabels.push(labelName);
          let issue = await editIssue({ issueId: this.state.issue.number, labels: addedLabels, state: this.state.issue.state });
          this.setState({ issue, isLoading: false });
          break;
        }
        case "remove": {
          let remainingLabels = this.state.issue.labels.filter(label => label.name !== labelName);
          let issue = await editIssue({ issueId: this.state.issue.number, labels: remainingLabels, state: this.state.issue.state });
          this.setState({ issue, isLoading: false });
          break;
        }
        case "toggleState": {
          let newState = this.state.issue.state === "open" ? "closed" : "open";
          let issue = await editIssue({ issueId: this.state.issue.number, labels: this.state.issue.labels, state: newState });
          this.setState({ issue, isLoading: false });
          break;
        }
        default: {
          break;
        }
      }
    } catch (error) {
      throw error;
    }
  };

  render() {
    let { classes } = this.props;
    let loading = this.state.isLoading ? <Loader /> : null;
    let comments = this.state.comments.map(comment => (
      <Card key={comment.id} className={classes.commentCard}>
        <CardHeader
          action={<DeleteIcon onClick={() => this.deleteCommentHandler(comment.id)} />}
          subheader={moment(comment.created_at).fromNow()}
          title={comment.user.login}
          avatar={<Avatar src={comment.user.avatar_url} />}
        />

        <CardContent>
          <Markdown markup={comment.body} />
        </CardContent>
      </Card>
    ));
    let content = !this.state.isLoading ? (
      <React.Fragment>
        <Card className={classes.issueCard}>
          <CardHeader
            avatar={<Avatar src={this.state.issue.user.avatar_url} />}
            subheader={`${this.state.issue.user.login} ${moment(this.state.issue.created_at).fromNow()}`}
            title={this.state.issue.title}
            action={
              <Button color="secondary" className={classes.button} onClick={() => this.editIssueHandler("toggleState")}>
                {this.state.issue.state.toUpperCase()}
              </Button>
            }
          />
          <div className={classes.labels}>
            {this.state.issue.labels.map(label => (
              <Chip
                key={label.id}
                icon={<LabelIcon style={{ color: "white" }} />}
                onDelete={() => this.editIssueHandler("remove", label.name)}
                style={{
                  backgroundColor: "#" + label.color,
                  color: "white",
                  marginRight: "0.2rem",
                  marginTop: "0.2rem"
                }}
                label={label.name}
              />
            ))}
            <FormControl className={classes.addLabel}>
              <InputLabel htmlFor="select-label">Add Label</InputLabel>
              <Select
                value={this.state.newLabel}
                onChange={e => this.editIssueHandler("add", e.target.value)}
                input={<Input name="label" id="select-label" />}
              >
                <MenuItem value="">Select</MenuItem>
                {this.state.labels.map(label => (
                  <MenuItem key={label.id} value={label.name}>
                    {label.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </div>
          <CardContent>
            <Markdown markup={this.state.issue.body} />
          </CardContent>
        </Card>
        {comments}
        <Card className={classes.commentCard}>
          <CardContent>
            <TextField
              className={classes.newComment}
              multiline
              rows="4"
              value={this.state.newComment}
              onChange={this.newCommentChangeHandler}
              margin="normal"
              variant="outlined"
            />
          </CardContent>

          <Button color="primary" onClick={this.addCommentHandler} className={classes.button}>
            Add Comment
          </Button>
        </Card>
      </React.Fragment>
    ) : null;
    return (
      <React.Fragment>
        <AppBar position="static" color="primary">
          <Toolbar>
            <IconButton className={classes.backButton} onClick={this.goBack} color="inherit">
              <ArrowBackIcon />
            </IconButton>
            <Typography variant="h5" color="inherit">
              #{this.props.match.params.issueId}
            </Typography>
          </Toolbar>
        </AppBar>
        {loading}
        {content}
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(IssueDetail);
