import React, { Component } from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import "typeface-roboto";

import NavBar from "./layout/NavBar/NavBar";
import IssuesList from "./components/IssuesList/IssuesList";

class App extends Component {
  state = {
    searchQuery: ""
  };

  searchStrChangeHandler = searchStr => {
    this.setState({ searchQuery: searchStr });
  };

  render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <NavBar searchStrChangeHandler={this.searchStrChangeHandler} />
        <IssuesList searchQuery={this.state.searchQuery} />
      </React.Fragment>
    );
  }
}

export default App;
