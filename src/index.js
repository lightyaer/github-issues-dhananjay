import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import App from "./App";
import IssueDetail from "./components/IssueDetail/IssueDetail";

const router = (
  <BrowserRouter>
    <Switch>
      <Route path="/" exact component={App} />
      <Route path="/issues/:issueId" exact component={IssueDetail} />
    </Switch>
  </BrowserRouter>
);

ReactDOM.render(router, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
