import axios from "../axiosInstance";

export const getIssues = async ({ sort = "created", state = "open", direction = "desc", labels = null, page = 1, per_page = 15, creator = null }) => {
  try {
    const issues = await axios.get("/repos/Lawrence4code/github-issues-clone/issues", {
      params: {
        sort,
        labels,
        page,
        per_page,
        direction,
        state,
        creator
      }
    });
    return issues.data;
  } catch (error) {
    throw error;
  }
};

export const getIssueById = async issueId => {
  try {
    const issue = await axios.get(`/repos/Lawrence4code/github-issues-clone/issues/${issueId}`);
    return issue.data;
  } catch (error) {
    throw error;
  }
};

export const editIssue = async ({ issueId, labels, state }) => {
  try {
    const issue = await axios.patch(`/repos/Lawrence4code/github-issues-clone/issues/${issueId}`, {
      labels,
      state
    });
    return issue.data;
  } catch (error) {
    throw error;
  }
};

export const getComments = async issueId => {
  try {
    const comments = await axios.get(`/repos/Lawrence4code/github-issues-clone/issues/${issueId}/comments`);
    return comments.data;
  } catch (error) {
    throw error;
  }
};

export const postComment = async (issueId, body) => {
  try {
    const comment = await axios.post(`/repos/Lawrence4code/github-issues-clone/issues/${issueId}/comments`, { body });
    return comment.data;
  } catch (error) {
    throw error;
  }
};

export const deleteComment = async commentId => {
  try {
    const comment = await axios.delete(`/repos/Lawrence4code/github-issues-clone/issues/comments/${commentId}`);
    return comment.data;
  } catch (error) {
    throw error;
  }
};

export const getLabels = async () => {
  try {
    const labels = await axios.get("/repos/Lawrence4code/github-issues-clone/labels");
    return labels.data;
  } catch (error) {
    throw error;
  }
};

export const deleteLabel = async labelName => {
  try {
    const label = await axios.delete(`/repos/Lawrence4code/github-issues-clone/labels/${labelName}`);
    return label.data;
  } catch (error) {
    throw error;
  }
};

export const searchIssues = async q => {
  try {
    const issues = await axios.get("/search/issues", {
      params: {
        q
      }
    });
    return issues.data;
  } catch (error) {
    throw error;
  }
};
