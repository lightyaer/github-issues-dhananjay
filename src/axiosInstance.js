import axios from "axios";
const myAxios = axios.create({
  baseURL: "https://api.github.com",
  headers: {
    Authorization: "token e471dbd681f93791fd96d50fb9b4d337f30a667d",
    Accept: "application/vnd.github.symmetra-preview+json"
  }
});

export default myAxios;
